var FormDropzone = function () {


    return {
        //main function to initiate the module
        init: function () {  

            Dropzone.options.myDropzone = {
                dictDefaultMessage: "",
                paramName: "document",
                uploadMultiple: false,
                init: function() {
                    this.on("addedfile", function(file) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");
                        var submitButton = Dropzone.createElement("<a href='javascript:;'' class='btn blue btn-sm btn-block'>Submit</a>");
                        
                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                          // Make sure the button click doesn't submit the form:
                          e.preventDefault();
                          e.stopPropagation();

                          // Remove the file preview.
                          _this.removeFile(file);
                          // If you want to the delete the file on the server as well,
                          // you can do the AJAX request here.
                        });

                        submitButton.addEventListener("click", function(e) {
                          var category_id = $('#category_id').val();
                          var formData_object = new FormData();
                          formData_object.append(document, file[0]);

                          console.log(paramName)
                        })

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                        file.previewElement.appendChild(submitButton);
                    });
                }            
            }
        }
    };
}();

jQuery(document).ready(function() {    
   FormDropzone.init();
});