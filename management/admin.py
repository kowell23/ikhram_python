from django.contrib import admin
from management.models import *

class BiroPackagesAdmin(admin.ModelAdmin):
	list_display = ('biro_member', 'plan_package', 'expired',)

class PlanPackagesAdmin(admin.ModelAdmin):
	list_display = ('name', 'logo', 'phone',)

# Register your models here.
admin.site.register(PlanPackages, PlanPackagesAdmin)
admin.site.register(BiroPackages, BiroPackagesAdmin)
admin.site.register(BiroMembers)
