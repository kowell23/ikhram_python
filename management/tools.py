from django.contrib.auth.models import User
from ikhram.models import *
from management.models import *

from datetime import *
import json


# connect mongodb
from pymongo import MongoClient

client = MongoClient()
mongoDb = client.ikhram

def create_umrahs(packet):
	
	result = list(mongoDb.umrahs.find({ "packet_id": packet.id }))

	cp = ConfigurationPackets.objects.get(agent_id=packet.agent_id)

	if len(result) == 0:
		umrahs = {}
		unset_hotel_rooms = {}

		inserted_room = []

		for room in packet.rooms():

			if not room.room_type_id in inserted_room:
				unset_hotel_rooms.update({
					"room_type_id": room.room_type_id,
					"name": room.room_type.name,
					"price": room.price,
					"kurs": room.kurs,
					"available_room": room.room_total
				})

				inserted_room.append(room.room_type_id)

		umrahs.update({ 
			"packet_id": packet.id,
			"agent_id": packet.agent_id,
			"name": packet.name,
			"status": packet.status,
			"description": packet.description,
			"mecca_hotel_name": packet.mecca_hotel_name,
			"medinah_hotel_name": packet.medinah_hotel_name,
			"mecca_hotel_rate": packet.mecca_hotel_rate,
			"medinah_hotel_rate": packet.medinah_hotel_rate,
			"term": packet.term,
			"last_allowed_book": cp.last_allowed_book,
			"airline_id": packet.airline_id,
			"transportation_id": packet.transportation_id,
			"packet_type": packet.packet_type,
			"status_date": packet.status_date,
			"recomendation": packet.recomendation,
			"dp_idr": cp.dp_idr,
			"dp_dollar": cp.dp_dollar
		})

		data_airline = None
		data_tour = None
		data_term = None

		if packet.tour_hotel_id:
			tour_hotel = []
			for i in packet.tour_hotel_id:
				tour_hotel.append(int(i))

			umrahs.update({
				"tour_hotel_id": tour_hotel
			})

		if packet.airline.template_airline_id:
			template_facility_airlines = TemplateFacilityAirlines.objects.filter(template_airline_id=packet.airline.template_airline_id)
			all_facility = []
			for i in template_facility_airlines:
				facility_airline = list(mongoDb.facility_airlines.find({ "_id": i.facility_airline_id }))[0]
				data_facility = {
					"name_facility": str(facility_airline['name']),
					"name_icon": str(facility_airline['name_icon']),
					"include": str(i.include),
					"description": str(i.description),
					"show": str(i.show)
				}

				all_facility.append(data_facility)

			data_airline = json.dumps({
				"name_folder": packet.airline.template_airline.name_folder,
				"detail_facility": all_facility
			})

		if packet.template_tour_id:
			template_facility_tours = TemplateFacilityTours.objects.filter(template_tour_id=packet.template_tour_id)
			all_facility = []
			for i in template_facility_tours:
				facility_tour = list(mongoDb.facility_tours.find({ "_id": i.facility_tour_id }))[0]
				data_facility = {
					"name_facility": str(facility_tour['name']),
					"name_icon": str(facility_tour['name_icon']),
					"include": str(i.include),
					"description": str(i.description),
					"show": str(i.show)
				}

				all_facility.append(data_facility)

			data_tour = json.dumps({
				"name_folder": packet.template_tour.name_folder,
				"detail_facility": all_facility
			})

		if packet.template_term_id:
			template_facility_terms = TemplateFacilityTerms.objects.filter(template_term_id=packet.template_term_id)
			all_facility = []
			for i in template_facility_terms:
				facility_term = list(mongoDb.facility_terms.find({ "_id": i.facility_term_id }))[0]
				data_facility = {
					"name_facility": str(facility_term['name']),
					"name_icon": str(facility_term['name_icon']),
					"include": str(i.include),
					"description": str(i.description),
					"show": str(i.show)
				}

				all_facility.append(data_facility)

			data_term = json.dumps({
				"name_folder": packet.template_term.name_folder,
				"detail_facility": all_facility
			})

		umrahs.update({
			"included_facility_airline": data_airline,
			"included_facility_tour": data_tour,
			"included_facility_term": data_term
		})
		
		day = 0
		departures = []
		for i in packet.rooms().filter(room_type_id=packet.rooms()[0].room_type_id):
			depgroup = i.departure_group()
			# departure2 = list(mongoDb.departures.find({ "departure_date": depgroup.departure_date, "agent_id": depgroup.agent_id}))[0]

			departure2 = list(mongoDb.departures.findAndModify({
				query: { "departure_date": depgroup.departure_date, "agent_id": depgroup.agent_id },
				update: {
					"$setOnInsert": { "departure_date": depgroup.departure_date, "agent_id": int(depgroup.agent_id), "departure_month_year": int(depgroup.departure_date.strftime('%Y%m')) }
				},
				new: true,
				upsert: true
			}))

			departures.append(departure2)
			day = depgroup.days

		umrahs.update({
			"departures": departures,
			"day": day,
			teamwork_ids: []
		})

		umrahs.update({ "$unset": inserted_room })


