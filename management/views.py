from django.shortcuts import get_object_or_404, render_to_response
from django.template import Context, Template, RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from ikhram.models import *
from management.models import *
from management import tools

from datetime import *
from django.db.models import Count
from random import *
from time import mktime

from schedular.tasks import *

from restclient import GET, POST, PUT, DELETE

# connect mongodb
from pymongo import MongoClient

client = MongoClient()
mongoDb = client.ikhram

# Create your views here.

@login_required
def index(request):
	dashboard_active = "open active"

	if request.user.is_staff:
		access_page_agents = TrackingPageAgents.objects.all()
		access_page_promotions = TrackingPagePromotions.objects.all()
		member_agents = BiroMembers.objects.all()

		packet_active = Packets.objects.filter(status=1)
		promotion_active = Promotions.objects.filter(status=1)
	else:
		access_page_agents = TrackingPageAgents.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id)
		access_page_promotions = TrackingPagePromotions.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id)
		data_leads = PurchaseLeads.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id)

		packet_active = Packets.objects.filter(status=1, agent_id=request.user.biromembers_set.all()[0].agent_id)
		promotion_active = Promotions.objects.filter(status=1, packet_id__in=packet_active.values_list('id', flat=True))

		chart_access_agents = []
		chart_access_promos = []

		for i in TrackingPageAgents.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id).extra({'created_at':"date(created_at)"}).values('created_at').annotate(total=Count('id')):
			chart_access_agents.append({ 'date': str(i['created_at'].strftime('%Y-%m-%d')), 'duration': int(i['total']) })

		for i in TrackingPagePromotions.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id).extra({'created_at':"date(created_at)"}).values('created_at').annotate(total=Count('id')):
			chart_access_promos.append({ 'date': str(i['created_at'].strftime('%Y-%m-%d')), 'duration': int(i['total']) })

	return render_to_response('dashboard.html', locals(), context_instance=RequestContext(request))

@login_required
def konfigurasi(request):
	config_active = "open active"

	try:
		cp = ConfigurationPackets.objects.get(agent_id=request.user.biromembers_set.all()[0].agent_id)
		action = "Edit"
	except Exception, e:
		cp = None
		action = "New"

	if request.POST:
		agent_id = request.POST.get('agent_id')
		last_allowed_book = request.POST.get('last_allowed_book')
		dp_idr = request.POST.get('dp_idr')
		dp_dollar = request.POST.get('dp_dollar')
		included_facility = request.POST.get('included_facility')
		excluded_facility = request.POST.get('excluded_facility')
		terms_condition = request.POST.get('terms_condition')

		if cp:

			try:
				cp.last_allowed_book = last_allowed_book
				cp.dp_idr = dp_idr
				cp.dp_dollar = dp_dollar
				cp.included_facility = included_facility
				cp.excluded_facility = excluded_facility
				cp.terms_condition = terms_condition
				cp.save()

				messages.success(request, "Konfigurasi berhasil diubah")
			except Exception, e:
				messages.error(request, str(e))

			return HttpResponseRedirect(reverse('konfigurasi'))

		else:

			try:
				cp = ConfigurationPackets()
				cp.agent_id = agent_id
				cp.last_allowed_book = last_allowed_book
				cp.dp_idr = dp_idr
				cp.dp_dollar = dp_dollar
				cp.included_facility = included_facility
				cp.excluded_facility = excluded_facility
				cp.terms_condition = terms_condition
				cp.save()

				messages.success(request, "Konfigurasi berhasil disimpan")
			except Exception, e:
				messages.error(request, str(e))

			return HttpResponseRedirect(reverse('konfigurasi'))

	return render_to_response('form_konfigurasi.html', locals(), context_instance=RequestContext(request))

@login_required
def data_leads(request):
	data_lead = "open active"

	datetime_now = datetime.now().strftime("%Y-%m-%d")

	customer_potentials = TrackUsers.objects.filter(status_purchase=0)
	customer_selfs = PurchaseLeads.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id)
	customer_other_biro = PurchaseLeads.objects.exclude(agent_id=request.user.biromembers_set.all()[0].agent_id)

	return render_to_response('data_leads.html', locals(), context_instance=RequestContext(request))

@login_required
def member_biro(request):
	member_biro_active = "open active"
	member_agents = BiroMembers.objects.all()

	return render_to_response('member_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def new_member_biro(request):

	member_biro_active = "open active"
	agents = Agents.objects.filter(status=1).exclude(id=2)

	if request.POST:
		agent_id = request.POST.get('agent_id')
		# email = request.POST.get('email')
		password  =request.POST.get('password')

		try:
			agent = Agents.objects.get(id=agent_id)

			user = User.objects.create_user(username=agent.main_contact_email, password=password)
			user.email = agent.main_contact_email
			user.save()
		except Exception, e:
			messages.error(request, str(e))
		else:
			bm = BiroMembers()
			bm.user = user
			bm.agent_id = agent_id
			bm.save()

			messages.success(request, "Member Biro Berhasil disimpan")

		return HttpResponseRedirect(reverse('member_biro'))

	return render_to_response('form_member_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def delete_member_biro(request, id):
	
	try:
		bm = BiroMembers.objects.get(id=id)
	except Exception, e:
		messages.error(request, str(e))
	else:

		if len(bm.biropackages_set.all()) > 0:
			bm.biropackages_set.all()[0].delete()

		bm.user.delete()
		bm.delete()

		messages.success(request, "Member Biro Berhasil dihapus")

	return HttpResponseRedirect(reverse('member_biro'))

@login_required
def profile(request):
	return render_to_response('profile.html', locals(), context_instance=RequestContext(request))

@login_required
def paket_biro(request):
	paket_biro_active = "open active"

	packet_agents = BiroPackages.objects.all()

	return render_to_response('paket_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def new_paket_biro(request):
	paket_biro_active = "open active"
	action = "new"

	member_agents = BiroMembers.objects.all()
	plan_packages = PlanPackages.objects.all()

	if request.POST:
		biro_member_id = request.POST.get('biro_member_id')
		packet_id = request.POST.get('packet_id')
		expired = request.POST.get('expired')
		split_expired = expired.split('-')

		try:
			BiroPackages.objects.get(biro_member_id=biro_member_id)
		except Exception, e:
			try:
				bp = BiroPackages()
				bp.biro_member_id = biro_member_id
				bp.plan_package_id = packet_id
				bp.expired = "%s-%s-%s" % (split_expired[2], split_expired[1], split_expired[0])
				bp.save()
			except Exception, e:
				messages.error(request, str(e))
			else:
				messages.success(request, "Biro Paket Berhasil disimpan")
		else:
			messages.error(request, "Biro sudah pernah diaktifkan paketnya")

		return HttpResponseRedirect(reverse('paket_biro'))

	return render_to_response('form_paket_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def edit_paket_biro(request, id):
	paket_biro_active = "open active"
	action = "edit"

	member_agents = BiroMembers.objects.all()
	plan_packages = PlanPackages.objects.all()

	bp = BiroPackages.objects.get(id=id)

	if request.POST:
		biro_member_id = request.POST.get('biro_member_id')
		packet_id = request.POST.get('packet_id')
		expired = request.POST.get('expired')
		split_expired = expired.split('-')

		
		try:
			bp.biro_member_id = biro_member_id
			bp.plan_package_id = packet_id
			bp.expired = "%s-%s-%s" % (split_expired[2], split_expired[1], split_expired[0])
			bp.save()
		except Exception, e:
			messages.error(request, str(e))
		else:
			messages.success(request, "Biro Paket Berhasil diubah")

		return HttpResponseRedirect(reverse('paket_biro'))

	return render_to_response('form_paket_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def delete_paket_biro(request, id):
	
	try:
		bp = BiroPackages.objects.get(id=id)
	except Exception, e:
		messages.error(request, str(e))
	else:

		bp.delete()

		messages.success(request, "Biro paket berhasil dihapus")

	return HttpResponseRedirect(reverse('paket_biro'))

@login_required
def paket_member(request):
	paket_active = "open active"

	packets = PlanPackages.objects.all()

	return render_to_response('paket_member.html', locals(), context_instance=RequestContext(request))

@login_required
def new_paket_member(request):
	paket_active = "open active"
	action = "new"
	
	if request.POST:
		name = request.POST.get('name')
		logo = request.POST.get('logo')
		phone = request.POST.get('phone')
		detailed_visitor_data = request.POST.get('detailed_visitor_data')
		promo_unlimited = request.POST.get('promo_unlimited')
		warranty_visitor_per_year = request.POST.get('warranty_visitor_per_year')
		minimal_discount = request.POST.get('minimal_discount')
		cost_per_year = request.POST.get('cost_per_year')
		commission = request.POST.get('commission')

		if logo == "1":
			new_logo = True
		else:
			new_logo = False

		if phone == "1":
			new_phone = True
		else:
			new_phone = False

		if detailed_visitor_data == "1":
			new_detailed_visitor_data = True
		else:
			new_detailed_visitor_data = False

		if promo_unlimited == "1":
			new_promo_unlimited = True
		else:
			new_promo_unlimited = False

		if warranty_visitor_per_year == "1":
			new_warranty_visitor_per_year = True
		else:
			new_warranty_visitor_per_year = False

		try:
			pp = PlanPackages()
			pp.name = name
			pp.logo = new_logo
			pp.phone = new_phone
			pp.detailed_visitor_data = new_detailed_visitor_data
			pp.promo_unlimited = new_promo_unlimited
			pp.warranty_visitor_per_year = new_warranty_visitor_per_year
			pp.minimal_discount = minimal_discount
			pp.cost_per_year = cost_per_year
			pp.commission = commission
			pp.save()

			messages.success(request, 'Paket member berhasil disimpan')
		except Exception, e:
			messages.error(request, str(e))

		return HttpResponseRedirect(reverse('paket_member'))

	return render_to_response('form_paket_member.html', locals(), context_instance=RequestContext(request))

@login_required
def edit_paket_member(request, id):
	paket_active = "open active"
	action = "edit"

	paket_member = PlanPackages.objects.get(id=id)
	
	if request.POST:
		name = request.POST.get('name')
		logo = request.POST.get('logo')
		phone = request.POST.get('phone')
		detailed_visitor_data = request.POST.get('detailed_visitor_data')
		promo_unlimited = request.POST.get('promo_unlimited')
		warranty_visitor_per_year = request.POST.get('warranty_visitor_per_year')
		minimal_discount = request.POST.get('minimal_discount')
		cost_per_year = request.POST.get('cost_per_year')
		commission = request.POST.get('commission')

		if logo == "1":
			new_logo = True
		else:
			new_logo = False

		if phone == "1":
			new_phone = True
		else:
			new_phone = False

		if detailed_visitor_data == "1":
			new_detailed_visitor_data = True
		else:
			new_detailed_visitor_data = False

		if promo_unlimited == "1":
			new_promo_unlimited = True
		else:
			new_promo_unlimited = False

		if warranty_visitor_per_year == "1":
			new_warranty_visitor_per_year = True
		else:
			new_warranty_visitor_per_year = False

		try:
			paket_member.name = name
			paket_member.logo = new_logo
			paket_member.phone = new_phone
			paket_member.detailed_visitor_data = new_detailed_visitor_data
			paket_member.promo_unlimited = new_promo_unlimited
			paket_member.warranty_visitor_per_year = new_warranty_visitor_per_year
			paket_member.minimal_discount = minimal_discount
			paket_member.cost_per_year = cost_per_year
			paket_member.commission = commission
			paket_member.save()

			messages.success(request, 'Paket member berhasil diubah')
		except Exception, e:
			messages.error(request, str(e))

		return HttpResponseRedirect(reverse('paket_member'))

	return render_to_response('form_paket_member.html', locals(), context_instance=RequestContext(request))

@login_required
def show_paket_member(request, id):
	paket_active = "open active"

	paket_member = PlanPackages.objects.get(id=id)

	return render_to_response('show_paket_member.html', locals(), context_instance=RequestContext(request))

@login_required
def delete_paket_member(request, id):
	try:
		pp = PlanPackages.objects.get(id=id)
	except Exception, e:
		messages.error(request, str(e))
	else:

		pp.delete()

		messages.success(request, "Pilihan paket member berhasil dihapus")

	return HttpResponseRedirect(reverse('paket_member'))

@login_required
def paket_umroh(request):
	paket_umroh_active = "open active"

	if request.user.is_staff:
		packet_umrohs = Packets.objects.filter(status=1)
	else:
		
		try:
			paket_member = BiroPackages.objects.get(biro_member__agent_id=request.user.biromembers_set.all()[0].agent_id)
		except BiroPackages.DoesNotExist:
			paket_member = None

		packet_umrohs = Packets.objects.filter(status=1, agent_id=request.user.biromembers_set.all()[0].agent_id)

	return render_to_response('paket_umroh.html', locals(), context_instance=RequestContext(request))

@login_required
def new_paket_umroh(request):
	paket_umroh_active = "open active"
	action = "new"

	PACKET_TYPE = (
		("REGULER", "Reguler"),
		("PLUSTOUR", "Plus Tour"),
		("PLUSTOKOH", "Plus Tokoh"),
	)

	KURS_CHOICES = (
		("$", "$"),
		("Rp.", "Rp."),
	)

	# airlines = Airlines.objects.filter(status=1).values_list('id').exclude(name__isnull=True).distinct()
	airlines = Airlines.objects.filter(status=1).exclude(name__isnull=True).exclude(name='-').values('id','name').annotate(total=Count('name'))
	transportations = Transportations.objects.filter(status=1).exclude(name__isnull=True).exclude(name='-').values('id','name').annotate(total=Count('name')).distinct()
	template_terms = TemplateTermsAgents.objects.all()
	template_tours = TemplateTours.objects.all()

	hotel_meccas = mongoDb.hotels.find({"location": "Mekkah"})
	hotel_medinahs = mongoDb.hotels.find({"location": "Medinah"})

	try:
		config = ConfigurationPackets.objects.get(agent_id=request.user.biromembers_set.all()[0].agent_id)
	except Exception, e:
		config = None

	if request.POST:

		# step 1 (Informasi Paket)
		agent_id = request.POST.get('agent_id')
		name_paket = request.POST.get('name_paket')
		maskapai_id = request.POST.get('maskapai_id')
		transportation_id= request.POST.get('transportation_id')
		day = request.POST.get('day')
		# last_allowed_book = request.POST.get('last_allowed_book')
		category_packet = request.POST.get('category_packet')
		kurs = request.POST.get('kurs')
		# dp_idr = request.POST.get('dp_idr')
		# dp_dollar = request.POST.get('dp_dollar')
		# template_tour_id = request.POST.get('template_tour_id')

		# step 2 (Kamar Paket)
		double_price = request.POST.get('double_price')
		double_total_kamar = request.POST.get('double_total_kamar')
		triple_price = request.POST.get('triple_price')
		triple_total_kamar = request.POST.get('triple_total_kamar')
		quadruple_price = request.POST.get('quadruple_price')
		quadruple_total_kamar = request.POST.get('quadruple_total_kamar')

		# step 3 (Fasilitas Paket)
		included_facility = request.POST.get('included_facility')
		excluded_facility = request.POST.get('excluded_facility')

		# step 4 (Jadwal Keberangkatan)
		departure_dates = request.POST.getlist('departure_date[]')

		# step 5 (Hotel Paket)
		hotel_meccas = request.POST.getlist('hotel_mecca[]')
		hotel_madinahs = request.POST.getlist('hotel_madinah[]')

		print hotel_meccas

		if config:
			
			try:
				p = Packets()
				p.agent_id = agent_id
				p.name = name_paket
				p.airline_id = maskapai_id
				p.transportation_id = transportation_id
				p.packet_type = "NORMAL"
				p.last_allowed_book = config.last_allowed_book
				p.dp_idr = config.dp_idr
				p.dp_dollar = config.dp_dollar
				p.status = 1
				p.save()
			except Exception, e:
				print "======= ERROR PAKET ============ %s ================ " % str(e)
				messages.error(request, str(e))

				return HttpResponseRedirect(reverse('paket_umroh'))
			else:

				for i in hotel_meccas:
					result = mongoDb.hotels.find({"location": "Mekkah", "name": str(i)})
					result_list = list(result)

					try:
						phmeccas = PacketHotelMeccas()
						phmeccas.packet_id = int(p.id)
						phmeccas.hotel_id = int(result_list[0]['_id'])
						phmeccas.rate = int(result_list[0]['rate'])
						phmeccas.save()
					except Exception, e:
						print "======= ERROR PAKET HOTEL MECCA ============ %s ================ " % str(e)

				for i in hotel_madinahs:
					result = mongoDb.hotels.find({"location": "Medinah", "name": str(i)})
					result_list = list(result)

					try:
						phmedinahs = PacketHotelMedinahs()
						phmedinahs.packet_id = int(p.id)
						phmedinahs.hotel_id = int(result_list[0]['_id'])
						phmedinahs.rate = int(result_list[0]['rate'])
						phmedinahs.save()
					except Exception, e:
						print "======= ERROR PAKET HOTEL MEDINAH ============ %s ================ " % str(e)


				for i in departure_dates:
					split_date = i.split('-')
					new_date = "%s-%s-%s" % (split_date[2], split_date[1], split_date[0])
					try:
						dg = DepartureGroups()
						dg.group_code = "GROUP%s" % int(str(int(mktime(datetime.now().timetuple())))[-3:])
						dg.agent_id = agent_id
						dg.days = day
						dg.status = 1
						dg.departure_date = new_date
						dg.save()
					except Exception, e:
						print "======= ERROR KEBERANGKATAN ============ %s ================ " % str(e)
					else:

						if double_price:

							try:
								r = Rooms()
								r.packet_id = int(p.id)
								r.room_type_id = 1
								r.departure_group_id = int(dg.id)
								r.price = double_price
								r.room_total = double_total_kamar if double_total_kamar else 10
								r.kurs = kurs
								r.save()
							except Exception, e:
								print "======= ERROR KAMAR DOUBLE ============ %s ================ " % str(e)

						if triple_price:

							try:
								r = Rooms()
								r.packet_id = int(p.id)
								r.room_type_id = 2
								r.departure_group_id = int(dg.id)
								r.price = triple_price
								r.room_total = triple_total_kamar if triple_total_kamar else 10
								r.kurs = kurs
								r.save()
							except Exception, e:
								print "======= ERROR KAMAR TRIPLE ============ %s ================ " % str(e)

						if quadruple_price:

							try:
								r = Rooms()
								r.packet_id = int(p.id)
								r.room_type_id = 3
								r.departure_group_id = int(dg.id)
								r.price = triple_price
								r.room_total = quadruple_total_kamar if quadruple_total_kamar else 10
								r.kurs = kurs
								r.save()
							except Exception, e:
								print "======= ERROR KAMAR QUADRUPLE ============ %s ================ " % str(e)

				status_celery = get_celery_worker_status()

				try:
					a = status_celery['ERROR']
				except Exception, e:
					# print "Celery Jalan"
					create_mongodb_umrahs.delay(p)
				else:
					
					url = 'http://admin.ikhram.com/api/managements/update_mongo_packet'
					# url = 'http://localhost:3000/api/managements/update_mongo_packet'
					data = { "packets": p.id }

					POST(url, params=data, headers={'Content-Type': 'application/json'})


				messages.success(request, "Paket umroh berhasil disimpan")

				return HttpResponseRedirect(reverse('paket_umroh'))
		else:

			messages.error(request, "Konfigurasi Paket Terlebih dahulu")

			return HttpResponseRedirect(reverse('konfigurasi'))

	return render_to_response('form_paket_umroh.html', locals(), context_instance=RequestContext(request))

@login_required
def delete_paket_umroh(request, id):

	try:
		p = Packets.objects.get(id=id)
	except Exception, e:
		messages.error(request, str(e))
	else:

		try:
			promotions = Promotions.objects.filter(packet_id=p.id)
		except Exception, e:
			print "================= DELETE PROMOTION GAGAL ============ %s " % str(e)
		else:

			for i in promotions:

				try:
					mongoDb.umrah_promotions.update({ 'promo_id': i.id }, { "$set":{ 'status': -1 }})
					i.status = -1
					i.save()
				except Exception, e:
					print "================= DELETE UMRAH PROMOTION GAGAL ============ %s " % str(e)

		try:
			mongoDb.umrahs.update({ 'packet_id': p.id},{ "$set":{'status': -1 }})
			p.status = -1
			p.save()
		except Exception, e:
			print "================= DELETE PAKET GAGAL ============ %s " % str(e)

		messages.success(request, "Paket umroh berhasil dihapus")

	return HttpResponseRedirect(reverse('paket_umroh'))

@login_required
def promosi_umroh(request):
	promosi_umroh_active = "open active"

	if request.user.is_staff:
		packet_umrohs = Promotions.objects.filter(status=1)
	else:

		try:
			paket_member = BiroPackages.objects.get(biro_member__agent_id=request.user.biromembers_set.all()[0].agent_id)
		except BiroPackages.DoesNotExist:
			paket_member = None

		packet_umrohs = Packets.objects.filter(status=1, agent_id=request.user.biromembers_set.all()[0].agent_id).values_list('id', flat=True)

		promotion_umrohs = Promotions.objects.filter(status=1, packet_id__in=packet_umrohs)

	return render_to_response('promotion_umroh.html', locals(), context_instance=RequestContext(request))

@login_required
def new_promosi_umroh(request):
	promosi_umroh_active = "open active"
	action = "New"

	packet_id = request.GET.get('packetId')

	if packet_id:
		packets = Packets.objects.get(status=1, agent_id=request.user.biromembers_set.all()[0].agent_id, id=packet_id)

		all_rooms = Rooms.objects.filter(packet_id=packets.id)
		rooms = all_rooms.values('room_type_id').annotate(total=Count('room_type_id'))

		room_type_name = []

		for i in rooms:

			if int(i['room_type_id']) == 1:
				room_type_name.append({
					"room_type_id": 1,
					"room_name": "Double"
				})

			elif int(i['room_type_id']) == 2:
				room_type_name.append({
					"room_type_id": 2,
					"room_name": "Triple"
				})

			elif int(i['room_type_id']) == 3:
				room_type_name.append({
					"room_type_id": 3,
					"room_name": "Quadruple"
				})

		departure_groups = Rooms.objects.filter(packet_id=packets.id).values_list('departure_group_id', flat=True)

		new_departure_groups = DepartureGroups.objects.filter(id__in=departure_groups).extra({'departure_date':"date(departure_date)"}).distinct()

		packet_kurs = all_rooms[0].kurs
	else:
		packets = Packets.objects.filter(status=1, agent_id=request.user.biromembers_set.all()[0].agent_id)

	if request.POST:

		availability = request.POST.get('availability')
		packetId = request.POST.get('packet_id')
		type_promo = request.POST.get('type_promo')
		promo_kurs = request.POST.get('promo_kurs')
		discount = request.POST.get('discount')
		room_promotions = request.POST.getlist('room_promotion[]')
		date_promotions = request.POST.getlist('date_promotion[]')
		tag_promos = request.POST.getlist('tag_promo[]')

		packets = Packets.objects.get(id=packetId)

		promotions_id = []

		for i in date_promotions:

			try:
				pt = PromotionTypes()
				pt.name = type_promo
				pt.value = discount
				pt.kurs = promo_kurs
				pt.agent_id = int(request.user.biromembers_set.all()[0].agent_id)
				pt.status = 1
				pt.save()
			except Exception, e:
				print "=============ERROR PROMOTION TYPE============%s==============" % str(e)

				messages.error(request, str(e))

				return HttpResponseRedirect("%s?packetId=%s" % (reverse('new_promosi_umroh'), packetId))
			else:

				try:

					promo_code = "PR%s" % str(int(mktime(datetime.now().timetuple())))[-4:]

					p = Promotions()
					p.status = 1
					p.availability = availability
					p.departure_group_id = int(i)
					p.expired_date = DepartureGroups.objects.get(id=i).departure_date
					p.packet_id = int(packetId)
					p.promotion_code = promo_code
					p.promotion_date = datetime.now()
					p.promotion_type_id = int(pt.id)
					p.title = "%s - %s" % (promo_code, packets.name)
					p.save()
				except Exception, e:
					print "=============ERROR PROMOTION============%s==============" % str(e)
					messages.error(request, str(e))

					return HttpResponseRedirect("%s?packetId=%s" % (reverse('new_promosi_umroh'), packetId))
				else:

					for r in room_promotions:

						try:
							pr = PromotionRooms()
							# pr.room_type_id = int(Rooms.objects.get(id=r).room_type_id)
							pr.room_type_id = int(r)
							pr.promotion_id = int(p.id)
							pr.original_price = Rooms.objects.get(packet_id=packetId, room_type_id=r, departure_group_id=i).price
							pr.ori_kurs = Rooms.objects.get(packet_id=packetId, room_type_id=r, departure_group_id=i).kurs
							pr.save()
						except Exception, e:
							print "=============ERROR PROMOTION ROOMS============%s==============" % str(e)

					promotions_id.append(p.id)

		status_celery = get_celery_worker_status()

		try:
			a = status_celery['ERROR']
		except Exception, e:
			# print "Celery Jalan"
			create_mongodb_umrah_promotions.delay(promotions_id)
		else:
			
			url = 'http://admin.ikhram.com/api/managements/update_mongo_promotion'
			# url = 'http://127.0.0.1:3000/api/managements/update_mongo_promotion'
			data = { "promotions": promotions_id }

			POST(url, params=data, headers={'Content-Type': 'application/json'})

		messages.success(request, "Promosi Berhasil disimpan")

		return HttpResponseRedirect(reverse('promosi_umroh'))

	return render_to_response('form_promosi_umroh.html', locals(), context_instance=RequestContext(request))

@login_required
def akses_halaman_biro(request):
	halaman_biro = "open active"

	start_date = request.GET.get('start_date')
	end_date = request.GET.get('end_date')

	if start_date and end_date:
		split_start_date = start_date.split('-')
		split_end_date = end_date.split('-')

		new_start_date = datetime.strptime("%s-%s-%s" % (split_start_date[2], split_start_date[1], split_start_date[0]), "%Y-%m-%d").date()
		new_end_date = datetime.strptime("%s-%s-%s" % (split_end_date[2], split_end_date[1], split_end_date[0]), "%Y-%m-%d").date() + timedelta(days=1)

		trackings = TrackingPageAgents.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id, created_at__gte=new_start_date, created_at__lte=new_end_date).extra({'created_at':"date(created_at)"}).values('created_at').annotate(total=Count('id'))

		chart_access_agents = []
		detail_access_agents = TrackingPageAgents.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id, created_at__gte=new_start_date, created_at__lte=new_end_date)

		for i in trackings:
			chart_access_agents.append({ 'date': str(i['created_at'].strftime('%Y-%m-%d')), 'duration': int(i['total']) })

	return render_to_response('akses_halaman_biro.html', locals(), context_instance=RequestContext(request))

@login_required
def akses_halaman_promo(request):
	halaman_promo = "open active"

	start_date = request.GET.get('start_date')
	end_date = request.GET.get('end_date')

	if start_date and end_date:
		split_start_date = start_date.split('-')
		split_end_date = end_date.split('-')

		new_start_date = datetime.strptime("%s-%s-%s" % (split_start_date[2], split_start_date[1], split_start_date[0]), "%Y-%m-%d").date()
		new_end_date = datetime.strptime("%s-%s-%s" % (split_end_date[2], split_end_date[1], split_end_date[0]), "%Y-%m-%d").date() + timedelta(days=1)

		trackings = TrackingPagePromotions.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id, created_at__gte=new_start_date, created_at__lte=new_end_date).extra({'created_at':"date(created_at)"}).values('created_at').annotate(total=Count('id'))

		chart_access_promos = []
		detail_access_promos = TrackingPagePromotions.objects.filter(agent_id=request.user.biromembers_set.all()[0].agent_id, created_at__gte=new_start_date, created_at__lte=new_end_date)

		for i in trackings:
			chart_access_promos.append({ 'date': str(i['created_at'].strftime('%Y-%m-%d')), 'duration': int(i['total']) })

	return render_to_response('akses_halaman_promo.html', locals(), context_instance=RequestContext(request))

@login_required
def customer_services(request):
	cs_active = "open active"

	customer_services = CustomerServiceAgents.objects.all()

	return render_to_response('customer_services.html', locals(), context_instance=RequestContext(request))

@login_required
def new_customer_services(request):
	cs_active = "open active"
	action = "New"

	agents = Agents.objects.filter(status=1).exclude(id=2)

	if request.POST:

		photo = request.FILES['photo']
		agent_id = request.POST.get('agent_id')
		name = request.POST.get('nama')
		phone = request.POST.get('phone')
		bbm = request.POST.get('bbm')
		wa = request.POST.get('wa')
		email = request.POST.get('email')
		phone_office = request.POST.get('phone_office')
		
		try:
			cs = CustomerServiceAgents()
			cs.agent_id = agent_id
			cs.photo = photo
			cs.name = name
			cs.email = email
			cs.phone = phone
			cs.bbm = bbm
			cs.wa = wa
			cs.phone_office = phone_office
			cs.save()

			messages.success(request, 'Customer Services berhasil disimpan')
		except Exception, e:
			messages.error(request, str(e))

		return HttpResponseRedirect(reverse('customer_services'))

	return render_to_response('form_customer_services.html', locals(), context_instance=RequestContext(request))

@login_required
def edit_customer_services(request, id):
	cs_active = "open active"
	action = "Edit"

	agents = Agents.objects.filter(status=1).exclude(id=2)

	cs = CustomerServiceAgents.objects.get(id=id)

	if request.POST:

		photo = request.FILES.get('photo')
		agent_id = request.POST.get('agent_id')
		name = request.POST.get('nama')
		phone = request.POST.get('phone')
		bbm = request.POST.get('bbm')
		wa = request.POST.get('wa')
		email = request.POST.get('email')
		phone_office = request.POST.get('phone_office')
		
		try:

			if photo:
				cs.photo = photo
			cs.name = name
			cs.email = email
			cs.phone = phone
			cs.bbm = bbm
			cs.wa = wa
			cs.phone_office = phone_office
			cs.save()

			messages.success(request, 'Customer Services berhasil diubah')
		except Exception, e:
			messages.error(request, str(e))

		return HttpResponseRedirect(reverse('customer_services'))

	return render_to_response('form_customer_services.html', locals(), context_instance=RequestContext(request))

@login_required
def show_customer_services(request, id):
	cs_active = "open active"

	cs = CustomerServiceAgents.objects.get(id=id)

	return render_to_response('show_customer_services.html', locals(), context_instance=RequestContext(request))

@login_required
def delete_customer_services(request, id):
	try:
		cs = CustomerServiceAgents.objects.get(id=id)
	except Exception, e:
		messages.error(request, "Customer Services gagal dihapus")
	else:

		cs.delete()

		messages.success(request, "Customer Services berhasil dihapus")

	return HttpResponseRedirect(reverse('customer_services'))

def get_celery_worker_status():
    ERROR_KEY = "ERROR"
    try:
        from celery.task.control import inspect
        insp = inspect()
        d = insp.stats()
        if not d:
            d = { ERROR_KEY: 'No running Celery workers were found.' }
    except IOError as e:
        from errno import errorcode
        msg = "Error connecting to the backend: " + str(e)
        if len(e.args) > 0 and errorcode.get(e.args[0]) == 'ECONNREFUSED':
            msg += ' Check that the RabbitMQ server is running.'
        d = { ERROR_KEY: msg }
    except ImportError as e:
        d = { ERROR_KEY: str(e)}
    return d
