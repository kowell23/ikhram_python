from django.db import models

from ikhram.models import *
from django.contrib.auth.models import User
from django.conf import settings
from django.template.defaultfilters import slugify

from datetime import *

# Create your models here.

class PlanPackages(models.Model):

	name = models.CharField(max_length=255, null=True)
	logo = models.BooleanField(default=False)
	phone = models.BooleanField(default=False)
	detailed_visitor_data = models.BooleanField(default=False)
	promo_unlimited = models.BooleanField(default=False)
	warranty_visitor_per_year = models.BooleanField(default=False)
	commission = models.CharField(max_length=255)
	minimal_discount = models.FloatField(default=0.0)
	cost_per_year = models.IntegerField(max_length=255)
	slug = models.SlugField(editable=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'plan_packages'
		verbose_name_plural = "Pilihan Paket"

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		self.slug = slugify(self.name)
		super(PlanPackages, self).save(*args, **kwargs)

class BiroMembers(models.Model):
	agent = models.ForeignKey(Agents)
	user = models.ForeignKey(User)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'biro_members'
		verbose_name_plural = "Member Biro"

	def __unicode__(self):
		return self.user.username

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		super(BiroMembers, self).save(*args, **kwargs)


class BiroPackages(models.Model):

	FORMAT_DATES = ['%Y-%m-%d', '%d-%m-%Y']

	biro_member = models.ForeignKey(BiroMembers)
	plan_package = models.ForeignKey(PlanPackages)
	expired = models.DateField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'biro_packages'
		verbose_name_plural = "Paket Biro"

	def __unicode__(self):
		return self.biro_member.agent.name

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		super(BiroPackages, self).save(*args, **kwargs)

class TemplateTermsAgents(models.Model):

	agent = models.ForeignKey(Agents)
	name = models.CharField(max_length=255, blank=True)
	name_folder = models.CharField(max_length=255, blank=True)
	slug = models.SlugField(editable=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'template_terms_agents'
		verbose_name_plural = "Template Syarat dan Ketentuan"

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		self.slug = slugify(self.name)
		super(TemplateTermsAgents, self).save(*args, **kwargs)

class ConfigurationPackets(models.Model):

	agent = models.ForeignKey(Agents)
	last_allowed_book = models.CharField(max_length=255, null=True)
	dp_idr = models.CharField(max_length=255, null=True)
	dp_dollar = models.CharField(max_length=255, null=True)
	terms_condition = models.TextField(null=True)
	included_facility = models.TextField(null=True)
	excluded_facility = models.TextField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'configuration_packets'
		verbose_name_plural = "Konfigurasi Paket"

	def __unicode__(self):
		return self.agent.name

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		super(ConfigurationPackets, self).save(*args, **kwargs)

class CustomerServiceAgents(models.Model):

	agent = models.ForeignKey(Agents)
	name = models.CharField(max_length=255, null=True)
	photo = models.ImageField(upload_to='uploads/customer_service', null=True)
	phone = models.CharField(max_length=255, null=True)
	bbm = models.CharField(max_length=255, null=True)
	wa = models.CharField(max_length=255, null=True)
	email = models.CharField(max_length=255, null=True)
	phone_office = models.CharField(max_length=255, null=True)
	slug = models.SlugField(editable=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'customer_service_agents'
		verbose_name_plural = "Customer Service Biro"

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.created_at == None:
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		self.slug = slugify(self.name)
		super(CustomerServiceAgents, self).save(*args, **kwargs)
