from django.conf.urls import patterns, include, url

urlpatterns = patterns('management.views',
	url(r'^$', 'index', name='dashboard_index'),
	url(r'^member_biro$', 'member_biro', name='member_biro'),
	url(r'^member_biro/new$', 'new_member_biro', name='new_member_biro'),
	url(r'^member_biro/delete/(?P<id>[0-9]+)$', 'delete_member_biro', name='delete_member_biro'),

	url(r'^profile$', 'profile', name='profile'),

	url(r'^data_leads$', 'data_leads', name='data_leads'),

	url(r'^paket_biro$', 'paket_biro', name='paket_biro'),
	url(r'^paket_biro/new$', 'new_paket_biro', name='new_paket_biro'),
	url(r'^paket_biro/edit/(?P<id>[0-9]+)$', 'edit_paket_biro', name='edit_paket_biro'),
	url(r'^paket_biro/delete/(?P<id>[0-9]+)$', 'delete_paket_biro', name='delete_paket_biro'),

	url(r'^paket_member$', 'paket_member', name='paket_member'),
	url(r'^paket_member/new$', 'new_paket_member', name='new_paket_member'),
	url(r'^paket_member/(?P<id>[0-9]+)$', 'show_paket_member', name='show_paket_member'),
	url(r'^paket_member/edit/(?P<id>[0-9]+)$', 'edit_paket_member', name='edit_paket_member'),
	url(r'^paket_member/delete/(?P<id>[0-9]+)$', 'delete_paket_member', name='delete_paket_member'),

	url(r'^paket_umroh$', 'paket_umroh', name='paket_umroh'),
	url(r'^paket_umroh/new$', 'new_paket_umroh', name='new_paket_umroh'),
	url(r'^paket_umroh/delete/(?P<id>[0-9]+)$', 'delete_paket_umroh', name='delete_paket_umroh'),

	url(r'^promosi_umroh$', 'promosi_umroh', name='promosi_umroh'),
	url(r'^promosi_umroh/new$', 'new_promosi_umroh', name='new_promosi_umroh'),

	url(r'^konfigurasi$', 'konfigurasi', name='konfigurasi'),

	url(r'^akses_halaman_biro$', 'akses_halaman_biro', name='akses_halaman_biro'),
	url(r'^akses_halaman_promo$', 'akses_halaman_promo', name='akses_halaman_promo'),

	url(r'^customer_services$', 'customer_services', name='customer_services'),
	url(r'^customer_services/new$', 'new_customer_services', name='new_customer_services'),
	url(r'^customer_services/(?P<id>[0-9]+)$', 'show_customer_services', name='show_customer_services'),
	url(r'^customer_services/edit/(?P<id>[0-9]+)$', 'edit_customer_services', name='edit_customer_services'),
	url(r'^customer_services/delete/(?P<id>[0-9]+)$', 'delete_customer_services', name='delete_customer_services'),
)