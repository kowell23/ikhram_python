#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import re
import logging
import json

from restclient import GET, POST, PUT, DELETE
from celery import task
from ikhram.models import *
from management.models import *

# connect mongodb
from pymongo import MongoClient

client = MongoClient()
mongoDb = client.ikhram

def send_sms(no, text):
    while True:
        # if '+62' in no:
        #     no = no.replace('+62', '0')
        # no = ''.join(re.findall(r'[\d]+', no))
        url = 'http://www.freesms4us.com/kirimsms.php'
        data = {'user':'ikhram2016', 'pass':'Ikhr4M2016', 'isi': text, 'no':no}
        data = urllib.urlencode(data)
        complete_url = url + '?' + data
        resp = GET(complete_url)
        # if 'GAGAL' not in resp:
        #     break

    # email_to('SMS BIRO HARIAN', json.dumps(["m.hidayatulloh1@gmail.com"]), 'Resp: %s<br><br>Text:%s' % (resp, text))

    return resp

@task()
def email_to(subject, to, msg, fr='Support Ikhram.com <support@ikhram.com>', headers=None):
    from django.core.mail import send_mail, EmailMessage
    try:
        subject = subject
        html_content = msg
        from_email = fr
        to = json.loads(to)
        if headers:
            headers = json.loads(headers)
        else:
            headers = {'Reply-To': 'support@ikhram.com'}

        msg = EmailMessage(subject, html_content, from_email, to, headers=headers)
        msg.content_subtype = "html"
        msg.send(fail_silently=False)
    except Exception, err:
        logging.exception('\nNotification Mail Failed: Exception: %s\n' % err)
        return False
    else:
        logging.info('\nEmail Status: Success to: %s\n' % to)
    return True

@task()
def send_mail_task():

    to_email_biros = Agents.objects.filter(status=1).exclude(id=2)

    jumlah_data = TrackUsers.objects.filter(status_purchase=0)

    sub = "%s pembeli potensial tertarik membeli paket umroh anda" % len(jumlah_data)

    for i in to_email_biros:
        text = "Assalamualaikum wr wb.<br>Yth pimpinan / pengelola %s<br><br>Kami telah mencatat ada %s yang tertarik membeli paket umroh melalui sistem kami. Untuk mengakses data tersebut silahkan buka alamat : %s.<br><br>Untuk top up jumlah kredit biro bapak/ibu, silahkan menggunakan menu top up di %s atau hubungi customer support kami melalui %s<br><br>Semoga data calon konsumen tersebut dapat membeli paket umroh melalui travel bapak/ibu.<br><br>Waalaikumsallam wr wb<br><br>" % (i.name, len(jumlah_data), '<a href="http://admin.ikhram.com">http://admin.ikhram.com</a>', '<a href="https://admin.ikhram.com/topup-kredit">https://admin.ikhram.com/topup-kredit</a>', '031-8479-331/0822-311-20002/0878-538-00002/0857-647-08000')

        email_to(sub, json.dumps([i.main_contact_email]), text)
    # email_to(sub, json.dumps(to_emails), text)
    # email_to(sub, json.dumps(["m.hidayatulloh1@gmail.com"]), "Testing schedular di server")

@task()
def send_mail_task_testing():

    jumlah_data = TrackUsers.objects.filter(status_purchase=0)

    sub = "%s pembeli potensial tertarik membeli paket umroh anda" % len(jumlah_data)

    email_to(sub, json.dumps(["m.hidayatulloh1@gmail.com"]), "Testing schedular di server")

@task()
def create_mongodb_umrahs(packet):
    url = 'http://admin.ikhram.com/api/managements/update_mongo_packet'
    # url = 'http://localhost:3000/api/managements/update_mongo_packet'
    data = { "packets": packet.id }

    POST(url, params=data, headers={'Content-Type': 'application/json'})

@task()
def create_mongodb_umrah_promotions(promotionsId):
    url = 'http://admin.ikhram.com/api/managements/update_mongo_promotion'
    # url = 'http://localhost:3000/api/managements/update_mongo_promotion'
    data = { "promotions": promotionsId }

    POST(url, params=data, headers={'Content-Type': 'application/json'})

@task()
def send_sms_task():

    # to_sms = Agents.objects.filter(status=1).exclude(id=2)

    to_sms = [
        { "telp": "081297805626", "name": "Bapak Mazni", "biro": "Hidayah Syafir" },
        { "telp": "08111115374", "name": "Bapak Rofiq", "biro": "Tursina Travel" },
        { "telp": "081296687999", "name": "Bapak Syamsul", "biro": "Globes Travel" },
        { "telp": "081310050444", "name": "Bapak H.Syatiri", "biro": "Kafilah Suci Travel" },
        { "telp": "08111486469", "name": "Bapak Tauhid", "biro": "Alhamdi Travel" },
        { "telp": "08129615605", "name": "Ibu Ida", "biro": "Nurmi Tour" },
        { "telp": "081293915820", "name": "Bapak H.Jafar", "biro": "Dua Ribu Wisata" },
        { "telp": "082111501566", "name": "Ibu Adiba", "biro": "Al Azhar Travel" },
        { "telp": "0811149571", "name": "Bapak H. Jamaluddin", "biro": "Al Bilad Travel" },
        { "telp": "085282660922", "name": "Bapak H. Ali", "biro": "Haromain Travel" },
        { "telp": "081280682043", "name": "Ibu Ana", "biro": "Maharani Tour" },
        { "telp": "081383907693", "name": "Bapak Andri", "biro": "Akram Travel" },
        { "telp": "08129219429", "name": "Ibu Intan ", "biro": "Awana Travel" },
        { "telp": "081218000045", "name": "Bapak Angga", "biro": "Babussalam Tour" },
        { "telp": "0821256601100", "name": "", "biro": "Badriyah Travel" },
        { "telp": "081564600102", "name": "Bapak H.Dede", "biro": "Salam Tour" },
        { "telp": "08176048333", "name": "", "biro": "Diansaltra Travel" },
        { "telp": "08161936860", "name": "Bapak H.Kadrie", "biro": "Al Aqsha Jisru Dakwah" },
        { "telp": "081282327709", "name": "", "biro": "Duta Faras" },
        { "telp": "087883454134", "name": "Bapak Edo", "biro": "Mahmuda Travel" },
        { "telp": "081280851851", "name": "Ibu Hj. Erika", "biro": "Annisa Travel" },
        { "telp": "081311534811", "name": "Bapak Fahmi", "biro": "RAMANI Travel" },
        { "telp": "0817709976", "name": "Ibu Hj. Saodah", "biro": "Farfaza Astatama" },
        { "telp": "085697215084", "name": "", "biro": "Altur Wisata" },
        { "telp": "081807089500", "name": "Bapak Tony Archam", "biro": "RH Wisata" },
        { "telp": "08128036734", "name": "Bapak Rahmaji", "biro": "Wahana Haji Umrah" },
        { "telp": "087889297910", "name": "Ibu Nur", "biro": "Munatour" },
        { "telp": "081513100202", "name": "Ibu Istanti", "biro": "Raudah Wisata" },
        { "telp": "08114441971", "name": "Bapak Hamim", "biro": "Pakem Tours" },
        { "telp": "08128436198", "name": "Bapak Hasan", "biro": "Gamalama Travel" },
        { "telp": "08128428165", "name": "", "biro": "Henira Tours" },
        { "telp": "081316837315", "name": "Bapak H.Herdian", "biro": "Iskandaria Tours" },
        { "telp": "0811910805", "name": "Bapak H.Azis", "biro": "Hira Tour" },
        { "telp": "081952317244", "name": "", "biro": "Ihya Tour" },
        { "telp": "081394788313", "name": "Ibu Iis", "biro": "Wahana Saabiq" },
        { "telp": "0817887477", "name": "Ibu Indira", "biro": "ICT Tours" },
        { "telp": "08128011845", "name": "", "biro": "Jawara Tours" },
        { "telp": "081932008315", "name": "Ibu Jihan", "biro": "Al Alamain Nusantara" },
        { "telp": "0818818457", "name": "Bapak Laksmono", "biro": "Relax Travel" },
        { "telp": "0811921787", "name": "Bapak H.Firman", "biro": "Maghfirah Travel" },
        { "telp": "0811865742", "name": "Bapak H. Magnatis", "biro": "Madina Tour" },
        { "telp": "085216772479", "name": "Bapak Mahdi", "biro": "Pastravel" },
        { "telp": "085310191910", "name": "Bapak Maksum", "biro": "Marco Travel" },
        { "telp": "085710074783", "name": "", "biro": "Goenawan Era Wisata" },
        { "telp": "081326728190", "name": "Ibu Munifah", "biro": "Namira Tour" },
        { "telp": "08131887357", "name": "Ibu Nurma", "biro": "Nayla Travel" },
        { "telp": "0811879111", "name": "Bapak Iqbal", "biro": "Nursa Travel" },
        { "telp": "081316049341", "name": "Bapak Nusron", "biro": "Hijaz Tour" },
        { "telp": "081807197000", "name": "Bapak H.Syam", "biro": "Patuna Travel" },
        { "telp": "08129810159", "name": "Bapak Aria", "biro": "Patih Indo" },
        { "telp": "0811796753", "name": "Bapak H.Faisal", "biro": "Patria Wisata" },
        { "telp": "08121057110", "name": "", "biro": "Phinisi Wisata" },
        { "telp": "081383392298", "name": "", "biro": "Qurshi Wisata" },
        { "telp": "0818963108", "name": "Bapak Arif", "biro": "Ria Tour" },
        { "telp": "08561215181", "name": "Bapak Roy", "biro": "Kanomas" },
        { "telp": "085882427500", "name": "Bapak Andre", "biro": "Samara Travel" },
        { "telp": "08111567844", "name": "Ibu Silvi", "biro": "Multazam Utama" },
        { "telp": "08119568567", "name": "Bapak Yani", "biro": "Sindo Wisata" },
        { "telp": "081807510440", "name": "", "biro": "bimtour" },
        { "telp": "085784969861", "name": "Ibu Mega", "biro": "SWI" },
        { "telp": "081390007117", "name": "Bapak H.Ali Mansur", "biro": "Tamasya Travel" },
        { "telp": "0811885535", "name": "Ibu Maya", "biro": "TIBI Tour" },
        { "telp": "083890454561", "name": "Ibu Umi", "biro": "Sakinah Travel" },
        { "telp": "081314757680", "name": "Ibu Widi", "biro": "Jamila Tours" },
        { "telp": "081320071001", "name": "Bapak Yogi", "biro": "Percikan Iman Tours" },
        { "telp": "085211939974", "name": "Bapak H.Zainuddin", "biro": "samawa travel" },
        { "telp": "081617191817", "name": "Bapak H.Tamam", "biro": "Zam-zam sumbula thoyyiba" },
        { "telp": "0818202548", "name": "Bapak H.Holil", "biro": "Noor Abika Travel" },
        { "telp": "0818901212", "name": "Bapak H. Budi", "biro": "Ahsanta Travel" },
        { "telp": "081905144259", "name": "Ibu Yandi Kondakh", "biro": "El Atieq Travel" },
        { "telp": "081288005581", "name": "Bapak H.Asnawi", "biro": "Mila Tour" },
        { "telp": "081284492555", "name": "", "biro": "Adzkira Travel" },
        { "telp": "087711777797", "name": "Bapak Chandra", "biro": "Al Amsor Travel" },
        { "telp": "082132549499", "name": "Ibu Wulandari", "biro": "Al Masha" },
        { "telp": "082125396745", "name": "Bapak Rifqi", "biro": "Bimalyndo Hajar Aswad" },
        { "telp": "08161332448", "name": "Bapak Rahma", "biro": "Damarwulan Tovelindo" },
        { "telp": "081331326677", "name": "Bapak H. Suyat", "biro": "Faliya Utama" },
        { "telp": "081387859600", "name": "Bapak H. Abdul Ghofar", "biro": "Ghfiary Tours" },
        { "telp": "081230328584", "name": "Bapak Aden", "biro": "zam-zam indah abadi" },
        { "telp": "0811110085", "name": "Ibu Dania Umar", "biro": "Dream Tour" },
        { "telp": "082110815577", "name": "Ibu Novi", "biro": "Tanjung Travel" },
        { "telp": "082233567793", "name": "Bapak Syarif", "biro": "Erni Tour" }
    ]

    jumlah_yang_disms = len(to_sms)

    jumlah_data = TrackUsers.objects.filter(status_purchase=0)

    for i in to_sms:

        if name == "":
            name_biro = "pimpinan / pengelola (%s)" % i["biro"]
        else:
            name_biro = "%s (%s)" % (i["name"], i["biro"])

        text = "Assalamualaikum wr wb. Yth %s, Kami telah mencatat ada %s yang tertarik membeli paket umroh melalui sistem kami. Silahkan buka alamat %s" % (name_biro, len(jumlah_data), 'http://admin.ikhram.com')
        send_sms(i["telp"], text)
