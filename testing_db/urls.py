from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ikhram.views.home', name='home'),
    url(r'^$', 'ikhram.views.login', name='login'),
    url(r'^logout/$', 'ikhram.views.logout', name='logout'),
	url(r'^dashboard/', include('management.urls')),   
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT })
)
