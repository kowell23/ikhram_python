"""
WSGI config for testing_db project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import sys
import site

# site.addsitedir('/path/to/your/virtualenv/lib/python2.6/site-packages')
# site.addsitedir('/web/ikhram_python_env/lib/python2.7/site-packages')

# sys.path.append('/web/')
# sys.path.append('/web/ikhram_python/')

# os.environ['DJANGO_SETTINGS_MODULE'] = 'test_db.settings'

# activate_this = "/web/ikhram_python_env/bin/activate_this.py"
# execfile(activate_this, dict(__file__=activate_this))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "testing_db.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
