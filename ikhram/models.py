# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models

from datetime import *
from django.template.defaultfilters import slugify


class ActiveAdminComments(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    resource_id = models.CharField(max_length=255)
    resource_type = models.CharField(max_length=255)
    author_id = models.IntegerField(blank=True, null=True)
    author_type = models.CharField(max_length=255, blank=True)
    body = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    namespace = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'active_admin_comments'


class AdminUsers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(unique=True, max_length=255)
    encrypted_password = models.CharField(max_length=255)
    reset_password_token = models.CharField(unique=True, max_length=255, blank=True)
    reset_password_sent_at = models.DateTimeField(blank=True, null=True)
    remember_created_at = models.DateTimeField(blank=True, null=True)
    sign_in_count = models.IntegerField(blank=True, null=True)
    current_sign_in_at = models.DateTimeField(blank=True, null=True)
    last_sign_in_at = models.DateTimeField(blank=True, null=True)
    current_sign_in_ip = models.CharField(max_length=255, blank=True)
    last_sign_in_ip = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    role_id = models.IntegerField(blank=True, null=True)
    confirmation_token = models.CharField(max_length=255, blank=True)
    confirmed_at = models.DateTimeField(blank=True, null=True)
    confirmation_sent_at = models.DateTimeField(blank=True, null=True)
    unconfirmed_email = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'admin_users'


class AgentPhotos(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    photo = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'agent_photos'


class Agents(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_code = models.CharField(max_length=255, blank=True)
    agent_logo = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    agent_address = models.CharField(max_length=255, blank=True)
    agent_phone = models.CharField(max_length=255, blank=True)
    main_contact = models.CharField(max_length=255, blank=True)
    main_contact_email = models.CharField(max_length=255, blank=True)
    main_contact_phone = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    via = models.CharField(max_length=255, blank=True)
    city_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(blank=True, null=True)
    state_id = models.IntegerField(blank=True, null=True)
    admin_user_id = models.IntegerField(blank=True, null=True)
    rating = models.FloatField(blank=True, null=True)
    agent_type = models.CharField(max_length=255, blank=True)
    legal_file = models.CharField(max_length=255, blank=True)
    enable_service_fitur = models.IntegerField(blank=True, null=True)
    partname = models.CharField(max_length=255, blank=True)
    license = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    start_price = models.FloatField(blank=True, null=True)
    status_import = models.IntegerField(blank=True, null=True)
    eilhamzah_id = models.IntegerField(blank=True, null=True)
    commission = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'agents'

    def __unicode__(self):
        return self.name


class Airlines(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    rate = models.FloatField(blank=True, null=True)
    template_airline_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'airlines'

    def template_airline(self):
        return TemplateAirlines.objects.get(pk=self.template_airline_id)


class ApiKeys(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    access_id = models.CharField(max_length=255, blank=True)
    private_key = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'api_keys'


class Articles(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    title = models.CharField(max_length=255, blank=True)
    short_description = models.TextField(blank=True)
    content = models.TextField(blank=True)
    status = models.IntegerField(blank=True, null=True)
    photo_article = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'articles'


class Authentications(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    web_user_id = models.IntegerField(blank=True, null=True)
    provider = models.CharField(max_length=255, blank=True)
    uid = models.CharField(max_length=255, blank=True)
    token = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'authentications'


class BailoutUmrohs(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    promotion_id = models.IntegerField(blank=True, null=True)
    code_bailout = models.CharField(max_length=255, blank=True)
    full_name = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    number_phone_home = models.CharField(max_length=255, blank=True)
    number_handphone = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    status = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    send_email = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'bailout_umrohs'


class BankPromotions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'bank_promotions'


class BilledVouchers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    voucher_id = models.IntegerField(blank=True, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    fee = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    agent_id = models.IntegerField(blank=True, null=True)
    packet_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'billed_vouchers'


class Billings(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    invoice_no = models.CharField(max_length=255, blank=True)
    bill_date = models.DateTimeField(blank=True, null=True)
    total = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    due_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    paid = models.IntegerField(blank=True, null=True)
    pre_billing_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'billings'


class Branches(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    phone = models.CharField(max_length=255, blank=True)
    contact_name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    city_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'branches'


class Bureaus(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    start_active_sk = models.DateTimeField(blank=True, null=True)
    end_active_sk = models.DateTimeField(blank=True, null=True)
    owner = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    state_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'bureaus'


class Cities(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    city_status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    state_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'cities'


class ConfirmationLetterAgents(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    confirmation_letter_id = models.IntegerField(blank=True, null=True)
    name_biro = models.CharField(max_length=255, blank=True)
    names_approved = models.CharField(max_length=255, blank=True)
    position = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'confirmation_letter_agents'


class ConfirmationLetters(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'confirmation_letters'


class CustomListDetails(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    custom_list_id = models.IntegerField(blank=True, null=True)
    promotion_id = models.IntegerField(blank=True, null=True)
    serial_number = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'custom_list_details'


class CustomLists(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    title = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'custom_lists'


class CustomerSupports(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    full_name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    category = models.CharField(max_length=255, blank=True)
    voucher_code = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'customer_supports'


class DepartureGroups(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    group_code = models.CharField(max_length=255, blank=True)
    departure_date = models.DateTimeField(blank=True, null=True)
    days = models.IntegerField(blank=True, null=True)
    expired_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    group_ref_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'departure_groups'

    def __unicode__(self):
        return '%s %s %s' % (self.agent_id, self.departure_date, self.days)


class DjangoContentType(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        # managed = False
        db_table = 'django_content_type'


class DjangoMigrations(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'django_migrations'


class Dokus(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    transidmerchant = models.CharField(max_length=125)
    totalamount = models.FloatField()
    words = models.CharField(max_length=200)
    statustype = models.CharField(max_length=1)
    response_code = models.CharField(max_length=50)
    approvalcode = models.CharField(max_length=6)
    trxstatus = models.CharField(max_length=50)
    payment_channel = models.IntegerField()
    paymentcode = models.IntegerField()
    session_id = models.CharField(max_length=48)
    bank_issuer = models.CharField(max_length=100)
    creditcard = models.CharField(max_length=16)
    payment_date_time = models.DateTimeField(auto_now_add=True)
    verifyid = models.CharField(max_length=30)
    verifyscore = models.IntegerField()
    verifystatus = models.CharField(max_length=10)
    installment_acquirer = models.CharField(max_length=255)
    promoid = models.CharField(max_length=255)
    tenor = models.CharField(max_length=255)

    class Meta:
        # managed = False
        db_table = 'dokus'


class Events(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    event_date = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    agent_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'events'


class ExcludeFacilities(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    default_value = models.IntegerField(blank=True, null=True)
    show_area = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'exclude_facilities'


class Facilities(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    default_value = models.IntegerField(blank=True, null=True)
    show_area = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'facilities'


class FacilityTerms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_icon = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'facility_terms'


class FacilityTours(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_icon = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'facility_tours'


class FacilityTransports(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_icon = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'facility_transports'


class FeaturedPictures(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    file_name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    promotion_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'featured_pictures'


class Hotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    rate = models.IntegerField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'hotels'


class Invites(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    subscriber_id = models.IntegerField(blank=True, null=True)
    email = models.CharField(unique=True, max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'invites'


class Mandiris(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    transaction_id = models.CharField(max_length=255, blank=True)
    response_code = models.CharField(max_length=255, blank=True)
    reciept_code = models.CharField(max_length=255, blank=True)
    response_desc = models.CharField(max_length=255, blank=True)
    voucher_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'mandiris'


class MemberGetMembers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    parent_id = models.IntegerField(blank=True, null=True)
    lft = models.IntegerField()
    rgt = models.IntegerField()
    subscriber_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'member_get_members'


class MemberPoints(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    web_user_id = models.IntegerField(blank=True, null=True)
    point = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'member_points'


class Messages(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    email = models.CharField(max_length=255, blank=True)
    email_body = models.TextField(blank=True)
    status = models.IntegerField(blank=True, null=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    sender = models.CharField(max_length=255, blank=True)
    sender_email = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'messages'


class NewsLatests(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    title = models.CharField(max_length=255, blank=True)
    short_description = models.TextField(blank=True)
    content = models.TextField(blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    images_news = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'news_latests'


class Notifications(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    notification_type = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True)
    notification_message = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'notifications'


class PacketEvents(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    event_date = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    packet_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'packet_events'


class PacketExcludeFacilities(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    exclude_facility_id = models.IntegerField(blank=True, null=True)
    packet_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'packet_exclude_facilities'


class PacketFacilities(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    facility_id = models.IntegerField(blank=True, null=True)
    packet_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'packet_facilities'


class PacketHotelMeccas(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    hotel_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'packet_hotel_meccas'


class PacketHotelMedinahs(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    hotel_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'packet_hotel_medinahs'


class PacketTeamworks(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    teamwork_id = models.IntegerField(blank=True, null=True)
    role_teamwork = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'packet_teamworks'


class Packets(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(null=True)
    name = models.CharField(max_length=255)
    airline_id = models.CharField(max_length=255, blank=True)
    mecca_hotel_rate = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    medinah_hotel_rate = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True, default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    transportation_id = models.IntegerField(blank=True, null=True)
    packet_type = models.CharField(max_length=255, blank=True)
    mecca_hotel_name = models.CharField(max_length=255, blank=True)
    medinah_hotel_name = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    packet_photo = models.CharField(max_length=255, blank=True)
    hotel_mecca_id = models.IntegerField(blank=True, null=True)
    hotel_medinah_id = models.IntegerField(blank=True, null=True)
    last_allowed_book = models.IntegerField(blank=True, null=True)
    term = models.TextField(blank=True)
    status_import = models.IntegerField(blank=True, null=True)
    packet_ref_id = models.CharField(max_length=255, blank=True)
    status_import_room = models.IntegerField(blank=True, null=True)
    file_brosur = models.CharField(max_length=255, blank=True)
    status_date = models.IntegerField(blank=True, null=True)
    recomendation = models.IntegerField(blank=True, null=True)
    dp_idr = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    dp_dollar = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    template_tour_id = models.IntegerField(blank=True, null=True)
    template_term_id = models.IntegerField(blank=True, null=True)
    tour_hotel_id = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = True
        db_table = 'packets'

    def __unicode__(self):
        return self.name

    def name_agent(self):
        agent = Agents.objects.get(id=self.agent_id)
        return agent.name

    def name_airline(self):
        airline = Airlines.objects.get(id=self.airline_id, status=1)
        return airline.name

    def name_transportation(self):
        transportation = Transportations.objects.get(id=self.transportation_id, status=1)
        return transportation.name

    def get_day(self):
        room = Rooms.objects.filter(packet_id=self.id)[0]
        depart = DepartureGroups.objects.get(id=room.departure_group_id)

        return depart.days

    def total_promotion(self):
        promotion = Promotions.objects.filter(packet_id=self.id, status=1)

        return promotion.count()

    def rooms(self):
        return Rooms.objects.filter(packet_id=self.id)

    def airline(self):
        return Airlines.objects.get(pk=self.airline_id)

    def template_tour(self):
        return TemplateTours.objects.get(pk=self.template_tour_id)

    def template_term(self):
        return TemplateTerms.objects.get(pk=self.template_term_id)


class PartnerAdvertisements(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    private_key = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'partner_advertisements'


class Payments(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    billing_id = models.IntegerField(blank=True, null=True)
    transfer_date = models.DateTimeField(blank=True, null=True)
    account_name = models.CharField(max_length=255, blank=True)
    bank = models.CharField(max_length=255, blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    notes = models.TextField(blank=True)
    receipt = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'payments'


class Pictures(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    filename = models.CharField(max_length=255, blank=True)
    reference_id = models.IntegerField(blank=True, null=True)
    reference_table = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'pictures'


class PreBillings(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    pre_invoice_no = models.CharField(max_length=255, blank=True)
    agent_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    total_commission = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'pre_billings'


class PromotionRooms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    room_type_id = models.IntegerField(blank=True, null=True)
    promotion_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    original_price = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    room_availability_ref_id = models.CharField(max_length=255, blank=True)
    ori_kurs = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'promotion_rooms'

    def __unicode__(self):
        return str(self.promotion_id)


class PromotionTypes(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    value = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    promo_type = models.CharField(max_length=255, blank=True)
    agent_id = models.IntegerField(blank=True, null=True)
    kurs = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'promotion_types'

    def __unicode__(self):
        return self.name


class Promotions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    departure_group_id = models.IntegerField(blank=True, null=True)
    promotion_type_id = models.IntegerField(blank=True, null=True)
    packet_id = models.IntegerField(blank=True, null=True)
    promotion_code = models.CharField(max_length=255, blank=True)
    availability = models.IntegerField(blank=True, null=True)
    unlimited = models.IntegerField(blank=True, null=True)
    expired_date = models.DateTimeField(blank=True, null=True)
    promotion_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    id_program_ref = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    rate_promo = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    additional_price = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'promotions'

    def __unicode__(self):
        return self.promotion_code

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Promotions, self).save(*args, **kwargs)

    def name_agent(self):
        packet = Packets.objects.get(id=self.packet_id)
        agent = Agents.objects.get(id=packet.agent_id)

        return agent.name

    def name_packet(self):
        packet = Packets.objects.get(id=self.packet_id)

        return packet.name

    def departure_date(self):
        departure_group = DepartureGroups.objects.get(id=self.departure_group_id)

        return departure_group.departure_date

    def double(self):
        room = PromotionRooms.objects.get(room_type_id=1, promotion_id=self.id)

        return room.original_price

    def triple(self):
        room = PromotionRooms.objects.get(room_type_id=2, promotion_id=self.id)

        return room.original_price

    def quadruple(self):
        room = PromotionRooms.objects.get(room_type_id=3, promotion_id=self.id)

        return room.original_price

    def kurs(self):
        room_kurs = PromotionRooms.objects.filter(promotion_id=self.id).values_list('ori_kurs', flat=True).distinct()[0]

        return room_kurs

class PurchaseLeads(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    track_user_id = models.IntegerField(blank=True, null=True)
    agent_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'purchase_leads'

    def __unicode__(self):
        return '%s - %s' % (self.agent_id, self.track_user_id)

    def track_user(self):
        return TrackUsers.objects.get(id=self.track_user_id)

    def agent(self):
        return Agents.objects.get(id=self.agent_id)

    def status_retur(self):
        datetime_now = datetime.now() + timedelta(days=32)
        minimum_retur = self.created_at + timedelta(days=30)
        
        if datetime_now > minimum_retur:
            return True
        else:
            return False

    def minimum_retur(self):
        return (self.created_at + timedelta(days=30)).strftime("%Y-%m-%d")

    def maximum_retur(self):
        return (self.created_at + timedelta(days=37)).strftime("%Y-%m-%d")


class Rates(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    recomendation = models.IntegerField(blank=True, null=True)
    description = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'rates'


class ReportedAgents(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    user_report_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'reported_agents'


class RestStatuses(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'rest_statuses'


class ReturnLeads(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    track_user_id = models.IntegerField(blank=True, null=True)
    agent_id = models.IntegerField(blank=True, null=True)
    reason = models.TextField(blank=True)
    other_reason = models.TextField(blank=True)
    status = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'return_leads'


class Roles(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    desc = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'roles'


class RoomTypes(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    capacity = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'room_types'


class Rooms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    room_type_id = models.IntegerField(blank=True, null=True)
    departure_group_id = models.IntegerField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    room_total = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    kurs = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'rooms'

    def room_type(self):
        return RoomTypes.objects.get(id=self.room_type_id)

    def departure_group(self):
        return DepartureGroups.objects.get(id=self.departure_group_id)


class SchemaMigrations(models.Model):
    version = models.CharField(unique=True, max_length=255)

    class Meta:
        # managed = False
        db_table = 'schema_migrations'


class Sessions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    session_id = models.CharField(max_length=255)
    data = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'sessions'


class SideDescriptions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    full_path = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'side_descriptions'


class SimpleCaptchaData(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    key = models.CharField(max_length=40, blank=True)
    value = models.CharField(max_length=6, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'simple_captcha_data'


class States(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    state_status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'states'


class Subscribers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    gift_voucher_code = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    token = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'subscribers'


class SubscribersBak(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    gift_voucher_code = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    token = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'subscribers_bak'


class SubscribersBak2(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    gift_voucher_code = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    token = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'subscribers_bak2'


class Suspends(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    suspend_reason = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'suspends'


class SystemParameters(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    code = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    value = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    value2 = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'system_parameters'


class Taggings(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    tag_id = models.IntegerField(blank=True, null=True)
    taggable_id = models.IntegerField(blank=True, null=True)
    taggable_type = models.CharField(max_length=255, blank=True)
    tagger_id = models.IntegerField(blank=True, null=True)
    tagger_type = models.CharField(max_length=255, blank=True)
    context = models.CharField(max_length=128, blank=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'taggings'


class Tags(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    taggings_count = models.IntegerField(blank=True, null=True)
    longword = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'tags'


class TariffSchemes(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    tariff = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    tariff_type = models.CharField(max_length=255, blank=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    packet_type = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    commision = models.FloatField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'tariff_schemes'


class Tariffs(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    bottom = models.IntegerField(blank=True, null=True)
    top = models.IntegerField(blank=True, null=True)
    tariff = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    packet_type = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'tariffs'


class Teamworks(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    image = models.CharField(max_length=255, blank=True)
    position = models.CharField(max_length=255, blank=True)
    experience = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'teamworks'


class TelkomselCustLoyals(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(max_length=255, blank=True)
    voucher_gift_id = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True)
    token = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'telkomsel_cust_loyals'


class TemplateAirlines(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    name_folder = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'template_airlines'


class TemplateFacilityAirlines(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    template_airline_id = models.IntegerField(blank=True, null=True)
    facility_airline_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    show = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_airlines'

    def template_airline(self):
        return TemplateAirlines.objects.get(pk=self.template_airline_id)


class TemplateFacilityHotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    template_hotel_id = models.IntegerField(blank=True, null=True)
    facility_hotel_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    show = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_hotels'


class TemplateFacilityTerms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    facility_term_id = models.IntegerField(blank=True, null=True)
    template_term_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    show = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_terms'


class TemplateFacilityTourHotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    facility_hotel_id = models.IntegerField(blank=True, null=True)
    template_tour_hotel_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    show = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_tour_hotels'


class TemplateFacilityTours(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    facility_tour_id = models.IntegerField(blank=True, null=True)
    template_tour_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    show = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_tours'


class TemplateFacilityTransports(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    facility_transport_id = models.IntegerField(blank=True, null=True)
    template_transport_id = models.IntegerField(blank=True, null=True)
    include = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    show = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_facility_transports'


class TemplateHotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    name_folder = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'template_hotels'


class TemplateTerms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_folder = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'template_terms'

    def agent(self):
        return Agents.objects.get(id=self.agent_id)


class TemplateTourHotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_folder = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_tour_hotels'


class TemplateTours(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_folder = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_tours'


class TemplateTransports(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    name_folder = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'template_transports'


class TopupHistories(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    total = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    status_payment = models.CharField(max_length=255, blank=True)
    added_total = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'topup_histories'


class Topups(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    total = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'topups'


class TourHotels(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    review_rate = models.FloatField(blank=True, null=True)
    template_tour_hotel_id = models.IntegerField(blank=True, null=True)
    included_facility = models.TextField(blank=True)
    slug = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'tour_hotels'


class TrackUserDetails(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    track_user_id = models.IntegerField(blank=True, null=True)
    url = models.CharField(max_length=255, blank=True)
    page_type = models.CharField(max_length=255, blank=True)
    page_title = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'track_user_details'

class TrackUsers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    web_user_id = models.IntegerField(blank=True, null=True)
    status_purchase = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    full_profile = models.TextField(blank=True)
    facebook_url = models.CharField(max_length=255, blank=True)
    twitter_url = models.CharField(max_length=255, blank=True)
    linkedin_url = models.CharField(max_length=255, blank=True)
    age = models.CharField(max_length=255, blank=True)
    economic = models.CharField(max_length=255, blank=True)
    never_go_umrah = models.CharField(max_length=255, blank=True)
    gender = models.CharField(max_length=255, blank=True)
    last_education = models.CharField(max_length=255, blank=True)
    merried = models.CharField(max_length=255, blank=True)
    description_profession = models.TextField(blank=True)

    class Meta:
        # managed = False
        db_table = 'track_users'

    def web_user(self):
        return WebUsers.objects.get(id=self.web_user_id)

    def last_access(self):

        track_user_detail = TrackUserDetails.objects.filter(track_user_id=self.id).order_by('-created_at')[0]

        return track_user_detail.created_at

class TrackingPageAgents(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    ip_address = models.CharField(max_length=255, blank=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'tracking_page_agents'

    def web_user_name(self):

        if self.web_user_id:
            return WebUsers.objects.get(id=self.web_user_id).name
        else:
            return "Non Member"

    def web_user_location(self):
        if self.web_user_id:
            return WebUsers.objects.get(id=self.web_user_id).location
        else:
            return "-"

class TrackingPagePromotions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    ip_address = models.CharField(max_length=255, blank=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    url = models.CharField(max_length=255, null=True)
    promotion_id = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'tracking_page_promotions'

    def web_user_name(self):

        if self.web_user_id:
            return WebUsers.objects.get(id=self.web_user_id).name
        else:
            return "Non Member"

    def web_user_location(self):
        if self.web_user_id:
            return WebUsers.objects.get(id=self.web_user_id).location
        else:
            return "-"

    def promotion(self):
        return Promotions.objects.get(id=self.promotion_id)


class Transportations(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'transportations'


class UserReports(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    agent_id = models.IntegerField(blank=True, null=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    depart_date = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'user_reports'


class Videos(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    title = models.CharField(max_length=255, blank=True)
    short_description = models.TextField(blank=True)
    content = models.TextField(blank=True)
    url_video = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    slug = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'videos'


class ViewedPackets(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    packet_id = models.IntegerField(blank=True, null=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'viewed_packets'


class ViewedPromotions(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    promotion_id = models.IntegerField(blank=True, null=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'viewed_promotions'


class VoucherGifts(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    code = models.CharField(unique=True, max_length=255, blank=True)
    nominal = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    status_send = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    status_confirmation = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'voucher_gifts'


class VoucherRooms(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    voucher_id = models.IntegerField(blank=True, null=True)
    room_type_id = models.IntegerField(blank=True, null=True)
    person_total = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # managed = False
        db_table = 'voucher_rooms'


class Vouchers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    promotion_id = models.IntegerField(blank=True, null=True)
    voucher_code = models.CharField(max_length=255, blank=True)
    full_name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    person_total = models.IntegerField(blank=True, null=True)
    adult_total = models.IntegerField(blank=True, null=True)
    cwb_total = models.IntegerField(blank=True, null=True)
    cnb_total = models.IntegerField(blank=True, null=True)
    infant_total = models.IntegerField(blank=True, null=True)
    total_cost = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    cancel_date = models.DateTimeField(blank=True, null=True)
    cancel_reason = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)
    approve_date = models.DateTimeField(blank=True, null=True)
    enable_service_fitur = models.IntegerField(blank=True, null=True)
    web_user_id = models.IntegerField(blank=True, null=True)
    voucher_location = models.CharField(max_length=255, blank=True)
    status_export = models.IntegerField(blank=True, null=True)
    pre_billing_status = models.IntegerField(blank=True, null=True)
    process_booking = models.IntegerField(blank=True, null=True)
    payment_method = models.CharField(max_length=255, blank=True)
    book_from = models.CharField(max_length=255, blank=True)
    payment_scheme = models.CharField(max_length=255, blank=True)
    payment_nominal = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    partner_key = models.CharField(max_length=255, blank=True)
    added_discount = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    gift_voucher_code = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'vouchers'


class WebUsers(models.Model):
    # id = models.IntegerField(primary_key=True)  # AutoField?
    email = models.CharField(unique=True, max_length=255)
    encrypted_password = models.CharField(max_length=255)
    reset_password_token = models.CharField(unique=True, max_length=255, blank=True)
    reset_password_sent_at = models.DateTimeField(blank=True, null=True)
    remember_created_at = models.DateTimeField(blank=True, null=True)
    sign_in_count = models.IntegerField(blank=True, null=True)
    current_sign_in_at = models.DateTimeField(blank=True, null=True)
    last_sign_in_at = models.DateTimeField(blank=True, null=True)
    current_sign_in_ip = models.CharField(max_length=255, blank=True)
    last_sign_in_ip = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    agent_id = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    role_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    image = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    location = models.CharField(max_length=255, blank=True)

    class Meta:
        # managed = False
        db_table = 'web_users'
