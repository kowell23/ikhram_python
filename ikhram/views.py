from django.shortcuts import get_object_or_404, render_to_response
from django.template import Context, Template, RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout
from django.contrib import auth
from .models import *

def login(request):
	next = request.GET.get('next')
	if request.POST:
		email = request.POST.get('email')
		password  =request.POST.get('password')

		try:
			user = auth.authenticate(username=email, password=password)
			if user.is_active:
				auth.login(request, user)
		except Exception, e:
			return HttpResponseRedirect('/')
		else:

			if next:
				return HttpResponseRedirect(next)
			else:
				return HttpResponseRedirect('/dashboard/')

	if request.user.is_authenticated():
		return HttpResponseRedirect('/dashboard/')

	return render_to_response('login.html', locals(), context_instance=RequestContext(request))

def logout(request):
	auth.logout(request)

	return HttpResponseRedirect('/')
